﻿using System;

namespace WeaponClass
{
    public class AbstractWeapon
    {
        private string _name;
        private float _weight;
        private int _cost;
        private int _quantity;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public float Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }
        public int Cost
        {
            get { return _cost; }
            set { _cost = value; }
        }
        public int Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }
    }

    public class MeleeWeapon : AbstractWeapon
    {
        private string _damageType;
        private float _length;
        public string DamageType
        {
            get { return _damageType; }
            set { _damageType = value; }
        }
        public float Length
        {
            get { return _length; }
            set { _length = value; }
        }
    }

    public class RangedWeapon : AbstractWeapon
    {
        private float _range;
        public float Range
        {
            get { return _range; }
            set { _range = value; }
        }
    }

    public class Rapier : MeleeWeapon
    {
        private string _guardType;
        public string GuardType
        {
            get { return _guardType; }
            set { _guardType = value; }
        }
    }

    public class Knife : MeleeWeapon
    {
        private string _sharpeningAngle;
        public string SharpeningAngle
        {
            get { return _sharpeningAngle; }
            set { _sharpeningAngle = value; }
        }
    }

    public class Rifle : RangedWeapon
    {
        private int _fireRate;
        public int FireRate
        {
            get { return _fireRate; }
            set { _fireRate = value; }
        }
    }

    public class Machinegun : RangedWeapon
    {
        private string _bulletType;
        public string BulletType
        {
            get { return _bulletType; }
            set { _bulletType = value; }
        }
    }
}
