﻿using System;
using WeaponClass;

namespace Division
{
    using WeaponFactory;
    public class Division
    {
        private int _weaponsNumber = 0;
        private AbstractWeapon[] weapons;
        private WeaponFactory weaponFactory = new WeaponFactory();

        public int WeaponsNumber
        {
            get { return _weaponsNumber; }
        }
        public AbstractWeapon GetWeapon(int i)
        {
            if (i > weapons.Length - 1)
            {
                return null;
            }
            return weapons[i];
        }
        public bool SetWeapon(int i, AbstractWeapon weapon)
        {
            if (i > weapons.Length - 1)
            {
                return false;
            }
            else
            {
                weapons[i] = weapon;
                return true;
            }
        }
        public Division(int weaponsNumber)
        {
            _weaponsNumber = weaponsNumber;
            weapons = new AbstractWeapon[weaponsNumber];
        }

        public void AddWeapon(AbstractWeapon weapon)
        {
            _weaponsNumber++;
            AbstractWeapon[] _weapons = new AbstractWeapon[_weaponsNumber];
            weapons.CopyTo(_weapons, 0);
            _weapons[_weaponsNumber - 1] = weapon;
            weapons = _weapons;
        }
        public bool DeleteWeapon(int i)
        {
            if (i > weapons.Length - 1)
            {
                return false;
            }
            else
            {
                AbstractWeapon[] _weapons = new AbstractWeapon[_weaponsNumber - 1];
                int currentIndex = 0;
                for (int j = 0; j < _weaponsNumber; j++)
                {
                    if (j != i)
                    {
                        _weapons[currentIndex] = weapons[j];
                        currentIndex++;
                    }
                }
                _weaponsNumber--;
                return true;
            }
        }

        public void Arm()
        {
            for (int i = 0; i < _weaponsNumber; i++)
            {
                if (i % 2 == 0)
                {
                    weapons[i] = weaponFactory.ProvideRifle();
                }
                else
                {
                    weapons[i] = weaponFactory.ProvideKnife();
                }
            }
        }
        public int GetCost()
        {
            int totalCost = 0;
            for (int i = 0; i < _weaponsNumber; i++)
            {
                totalCost += weapons[i].Cost * weapons[i].Quantity;
            }
            return totalCost;
        }
    }
}
