﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsApp
{
    public partial class Form1 : Form
    {
        public WeaponClass.AbstractWeapon weapon;
        public Division.Division division;
        private int editingIndex;
        private bool isEditing = false;
        private AddForm.Form1 addForm;
        private bool addFormOpen = false;
        public Form1()
        {
            InitializeComponent();
            AddForm.Form1.editingFinished += UpdateList;
            division = new Division.Division(0);
            label2.Text = division.GetCost().ToString();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!addFormOpen)
            {
                addForm = new AddForm.Form1();
                addForm.Activate();
                addForm.Show();
                addFormOpen = true;
            }
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            if (!addFormOpen)
            {
                weapon = division.GetWeapon(listBox1.SelectedIndex);
                editingIndex = listBox1.SelectedIndex;
                isEditing = true;
                addForm = new AddForm.Form1(weapon);
                addForm.Activate();
                addForm.Show();
                addFormOpen = true;
            }
        }
        private void UpdateList(WeaponClass.AbstractWeapon weapon)
        {
            addFormOpen = false;
            if (weapon == null)
            {
                return;
            }
            if (isEditing)
            {
                division.SetWeapon(editingIndex, weapon);
                isEditing = false;
            }
            else
            {
                division.AddWeapon(weapon);
            }
            UpdateListBox();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                division.DeleteWeapon(listBox1.SelectedIndex);
                UpdateListBox();
            }
        }

        private void UpdateListBox()
        {
            listBox1.Items.Clear();
            for (int i = 0; i < division.WeaponsNumber; i++)
            {
                WeaponClass.AbstractWeapon _weapon = division.GetWeapon(i);
                listBox1.Items.Add(_weapon.Name + "--" + _weapon.Cost + "--" + _weapon.Quantity);
            }
            label2.Text = division.GetCost().ToString();
        }
    }
}
