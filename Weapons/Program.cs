﻿using System;


namespace Weapons
{
    using Division;
    class Program
    {
        static void Main(string[] args)
        {
            Division division = new Division(3000);
            division.Arm();
            Console.WriteLine(division.GetCost());
        }
    }
}
